
/**
 * Klasa dodaje ActionListener do dwóch przycisków: 
 * - Clear - wyśwetla wzór rownania i usuwa wpisane współczynniki)
 * - Solve - podaje wartości szukanych pierwiastków lub wyświetla błąd
 * @author Przemysław Sterniński
 *
 */

public class EquationController {
	
	private final EquationModel model;
	private final EquationView widok;
	
	
	/**
	 * Konstruktor
	 * @param model obiekt klasy EquationModel
	 * @param widok obeikt klasy EquationView
	 */
	public EquationController(EquationModel model, EquationView widok){
		this.model = model;
		this.widok = widok;
		
		
		widok.getbClear().addActionListener(
											e -> {
													widok.domyslneUstawieniaOpisu(widok.getWzorRownania());
													widok.setTextWspA(null);
													widok.setTextWspB(null);
													widok.setTextWspC(null);
											});
		
		
		
		widok.getbSolve().addActionListener(
											e -> {
													double a, b, c;
													
													try{
														a = Double.parseDouble(widok.getTextWspA());
														b = Double.parseDouble(widok.getTextWspB());
														c = Double.parseDouble(widok.getTextWspC());
													
													}catch (NumberFormatException ex){
														widok.ustawieniaOpisuBlad("Number format error!");
														return;
													}
													
													if (a == 0){
														widok.ustawieniaOpisuBlad("Not a quadratic equation!");
														return;
													}
													
													widok.domyslneUstawieniaOpisu(model.rownanieKwadratowe(a, b, c));
											});
	}
	
}
