

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


/**
 * Klasa określająca wygląd programu.
 * @author Przemysław Sterniński
 *
 */
public class EquationView
	extends JFrame{
	
	private final JLabel opis;
	private final JButton bSolve;
	private final JButton bClear;
	private final JTextField wspA;
	private final JTextField wspB;
	private final JTextField wspC;
	
	
	/**
	 * Konstruktor.
	 * Dla przycisków ustawione są mnemoniki: Clear (Alt+c), Solve (Alt+s).
	 * Dla pól tekstowych określono ToolTipText np. dla pola współczynnika a - "Enter a".
	 */
	public EquationView() {
		super("Quadratic Equation v1.0");
		setPreferredSize(new Dimension(400, 150));
		
		JPanel panel = new JPanel();
		
		opis = new JLabel();
		domyslneUstawieniaOpisu(getWzorRownania());
		
		bSolve = utworzPrzycisk("Solve", 's');
		bClear = utworzPrzycisk("Clear", 'c');
		
		wspA = utworzPoleTxt("Enter a");
		wspB = utworzPoleTxt("Enter b");
		wspC = utworzPoleTxt("Enter c");
		
		panel.setLayout(new GridLayout(1, 0));
		panel.add(wspA);
		panel.add(wspB);
		panel.add(wspC);
		
		setLayout(new GridLayout(0, 1));
		Container cp = getContentPane();
		cp.add(opis);
		cp.add(panel);
		cp.add(bSolve);
		cp.add(bClear);

		opis.setOpaque(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
	}
	
	

	private JTextField utworzPoleTxt(String toolTip){
		JTextField txt = new JTextField();
		txt.setHorizontalAlignment(JTextField.CENTER);
		txt.setToolTipText(toolTip);
		return txt;
	}
	
	
	
	private JButton utworzPrzycisk(String nazwa, char memo){
		JButton b = new JButton(nazwa);
		b.setMnemonic(memo);
		return b;
	}
	
	
	/**
	 * Metod określa domyślne ustawienia dla etykiety w górnej części programu: kolor tła, kolor czcionki, tekst, położenie tekstu
	 * @param tekst tekst jaki się pojawi na etykiecie przy jego uruchomieniu lub po naciśnięciu przycisku Clear.
	 */
	public void domyslneUstawieniaOpisu(String tekst){
		opis.setBackground(Color.WHITE);
		opis.setForeground(Color.BLUE);
		opis.setText(tekst);	
		opis.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	
	
	/**
	 * Metod określa ustawienia dla etykiety w górnej części programu, w przypadku błędu: kolor tła, kolor czcionki, tekst, położenie tekstu
	 * @param tekst tekst jaki się pojawi na etykiecie w przypadku błedu.
	 */
	public void ustawieniaOpisuBlad(String tekst){
		opis.setBackground(Color.RED);
		opis.setForeground(Color.BLACK);
		opis.setText(tekst);
		opis.setHorizontalAlignment(SwingConstants.CENTER);
	}

	
	
	public JButton getbSolve() {
		return bSolve;
	}

	
	
	public JButton getbClear() {
		return bClear;
	}

	
	
	public JLabel getOpis() {
		return opis;
	}

	
	
	public String getTextWspA() {
		return wspA.getText();
	}

	
	
	public String getTextWspB() {
		return wspB.getText();
	}

	
	
	public String getTextWspC() {
		return wspC.getText();
	}
	

	
	public void setTextWspA(String txt) {
		wspA.setText(txt);
	}

	
	
	public void setTextWspB(String txt) {
		wspB.setText(txt);
	}

	
	
	public void setTextWspC(String txt) {
		wspC.setText(txt);
	}
	
	
	public String getWzorRownania() {
		return "<html>a*x1<sup>2</sup> + b*x2 + c </html>";
	}

}
