import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Zawarta w tej klasie metoda rownanieKwadratowe wylicza pierwiastki równania kawadratowego, na podstawie podanych współczynników
 * @author Przemysław Sterniński
 *
 */

public class EquationModel {

	
	/**
	 * Zwraca pierwiastki równania kwadratowego 
	 * @param a współczynnik a z wzoru a*x1^2 + b*x2 + c
	 * @param b współczynnik b z wzoru a*x1^2 + b*x2 + c
	 * @param c współczynnik c z wzoru a*x1^2 + b*x2 + c
	 * @return pierwiastki równania kwadratowego w formie tekstu
	 */
	public String rownanieKwadratowe(double a, double b, double c){
		double delta, x1, x2;
		DecimalFormat df = new DecimalFormat("#.####");
		df.setRoundingMode(RoundingMode.HALF_UP);
		String wynik;
		
		
		if (a == 0) {
			wynik = "Not a quadratic equation!";
			return wynik;
		}
		
		delta = b*b - 4*a*c;
		if (delta < 0){
			wynik = "no real roots";
		
		}else if(delta == 0){
			x1 = -b / (2*a);
			wynik = "x1 = x2 = " + df.format(x1);
		
		}else{
			x1 = (-b - Math.sqrt(delta)) / (2*a);
			x2 = (-b + Math.sqrt(delta)) / (2*a);
			wynik = "x1 = " + df.format(x1)
					+",    x2 = " + df.format(x2);
		}
		
		return wynik;
	}
	
}
