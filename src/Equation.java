

/**
 * Aplikacja wspomagająca rozwiązywanie równania kwadratowego a*x1^2 + b*x2 + c.
 * W środkowej części programu w polach tekstowych należy wpisać wpółczynnikia a, b, c dla wybranego równania.
 * Po wściśnięciu przycisku Solve (Alt + s) pojawi się rozwiązanie na górnej etykiecie, 
 * w przypadku błędu pojawi się komunikat na czerwonym tle. Przycisk Clear (Alt + c) przywraca domyślne ustawienia.
 * @author Przemysław Sterniński
 *
 */
public class Equation {

	public static void main(String[] args) {
		
		EquationModel model = new EquationModel();
		EquationView widok = new EquationView();
		EquationController kontroler = new EquationController(model, widok);
		
		widok.setVisible(true);

	}

}
